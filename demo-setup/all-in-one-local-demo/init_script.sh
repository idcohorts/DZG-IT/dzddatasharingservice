#!/bin/bash
echo "# Install tools inside container..."
docker exec nextcloud1 /bin/sh -c "apt update && apt install -y sudo jq"
echo "# Disable all Nextcloud apps..."
ALL_APP_NAMES=$(docker exec nextcloud1 /bin/sh -c "sudo -E -u www-data php occ app:list --output=json | jq -r '.[\"enabled\"] | keys | join(\",\")'")
IFS=', ' read -r -a ALL_APP_NAMES_ARRAY <<<"$ALL_APP_NAMES"
for APP_NAME in ${ALL_APP_NAMES_ARRAY[@]}; do docker exec nextcloud1 /bin/sh -c "sudo -E -u www-data php occ app:disable ${APP_NAME}"; done
echo "# Enable required apps..."
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ app:enable -f {activity,files_downloadactivity,files_trackdownloads,files_rightclick,files_automatedtagging,files_retention,files_sharing,systemtags,encryption,logreader,serverinfo,password_policy,privacy,registration,terms_of_service,theming,drop_account,user_retention,end_to_end_encryption}"
echo "# Add 'expire_file'-tag..."
TAG_ID=$(docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ tag:add expire_file invisible --output=json | jq -r '.[\"id\"]'")
echo "# Add automated filetagging workflow for tag 'expire_file' with id '${TAG_ID}'..."
docker exec nextcloud1 /bin/bash -c "curl -v -L -X POST 'http://localhost:80/ocs/v2.php/apps/workflowengine/api/v1/workflows/global?format=json' \
  -u admin:TOPSECRET_ADMIN \
  -H 'OCS-APIRequest: true' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Origin: http://localhost:80' \
  -H 'Connection: close' \
  --data-raw '{\"class\":\"OCA\\\FilesAutomatedTagging\\\Operation\",\"entity\":\"OCA\\\WorkflowEngine\\\Entity\\\File\",\"events\":[],\"name\":\"\",\"checks\":[{\"class\":\"OCA\\\WorkflowEngine\\\Check\\\UserGroupMembership\",\"operator\":\"!is\",\"value\":\"admin\",\"invalid\":false}],\"operation\":\"${TAG_ID}\",\"valid\":true}'"
echo ""
echo "# Add automated file retention..."
echo "run hotfix for https://github.com/nextcloud/files_retention/issues/254"
../../files_retention_hotfix.sh
docker exec nextcloud1 /bin/bash -c "curl -v -L -X POST 'http://localhost:80/ocs/v2.php/apps/files_retention/api/v1/retentions' \
  -u admin:TOPSECRET_ADMIN \
  -H 'OCS-APIRequest: true' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Origin: http://localhost:80' \
  -H 'Connection: close' \
  --data-raw '{\"tagid\":${TAG_ID},\"timeamount\":30,\"timeunit\":0,\"timeafter\":1}'"
echo ""
echo "# Set activity_expire_days to one year..."
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ config:system:set activity_expire_days --value=356 --type=integer"
echo "# Set activity logging for external mounts to true ('activity_use_cached_mountpoints=true')..."
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ config:system:set activity_use_cached_mountpoints --value=true --type=boolean"
echo "# Enable file encryption..."
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ encryption:enable"
echo "# Set background job to 'cron'"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ background:cron"
echo "Set sharing settings"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_allow_links --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_enabled --value=yes"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_allow_resharing --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_allow_group_sharing --value=yes"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_only_share_with_group_members --value=yes"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_exclude_groups --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_allow_share_dialog_user_enumeration --value=yes"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_restrict_user_enumeration_to_group --value=yes"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_restrict_user_enumeration_to_phone --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_restrict_user_enumeration_full_match --value=yes"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_default_permissions --value=1"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_default_permission_canupdate --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core outgoing_server2server_share_enabled --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core outgoing_server2server_group_share_enabled --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core incoming_server2server_share_enabled --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core incoming_server2server_group_share_enabled --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core lookupServerUploadEnabled --value=no"
echo "Set user registration settings"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration enabled --value=yes"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration show_phone --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration enforce_phone --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration domains_is_blocklist --value=''"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration show_domains --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration email_is_optional --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration disable_email_verification --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration email_is_login --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration show_fullname --value=yes"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration additional_hint --value='Please provide your email and full name that we can validate your affiliation.'"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration admin_approval_required --value=yes"
