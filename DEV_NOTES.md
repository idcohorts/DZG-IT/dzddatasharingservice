# Terms of conditions curl call
need ocs api suport https://github.com/nextcloud/terms_of_service/issues/258
```
docker exec nextcloud1 /bin/bash -c "curl -v -L -X POST 'http://localhost:8082/apps/terms_of_service/terms' \
  -u admin:TOPSECRET_ADMIN \
  -H 'OCS-APIRequest: true' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Origin: http://localhost:80' \
  --data-raw '{\"countryCode\":\"--\",\"languageCode\":\"en\",\"body\":\"Dies is eine blabla2\"}'
"
```