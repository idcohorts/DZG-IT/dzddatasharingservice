Datenschutzkonformer Austausch von pseudonymisierten medizinischen Daten /Forschungsdaten mit dem <MY ORGANISAION> Data Share Service (<MY_SERVICES_SHORTNAME>)

DATENSCHUTZERKLÄRUNG


I. NAME UND ANSCHRIFT DES FÜR DIE VERARBEITUNG VERANTWORTLICHEN
Verantwortlicher im Sinne der Datenschutz-Grundverordnung (DSGVO) und anderer nationaler Datenschutzgesetze in den Mitgliedsstaaten sowie anderer datenschutzrechtlicher Bestimmungen ist:
<MY ORGANISAION>
<MY ORGANISAION ADDRESS>
<MY ORGANISAION CONTACTS>


II. NAME UND ANSCHRIFT DES DATENSCHUTZBEAUFTRAGTEN
<MY ORGANISAION DATA PROTECTION OFFICER>
<MY ORGANISAION DATA PROTECTION OFFICER CONTACT>


III. ALLGEMEINE INFORMATIONEN ZUR DATENVERARBEITUNG
1.	UMFANG DER VERARBEITUNG VON PERSONENBEZOGENEN DATEN
Wir verarbeiten personenbezogene Daten unserer Nutzer grundsätzlich nur, soweit dies im Rahmen des Funktionierens des <MY ORGANISAION> Data Share Service (<MY_SERVICES_SHORTNAME>) erforderlich ist. Personenbezogene Daten unserer Nutzer werden in der Regel nur mit Einwilligung des Nutzers verarbeitet. Eine Ausnahme gilt in den Fällen, in denen eine vorherige Einwilligung aus sachlichen Gründen nicht eingeholt werden kann und gesetzliche Bestimmungen die Verarbeitung der Daten erlauben.

2.	DIE RECHTSGRUNDLAGE FÜR DIE VERARBEITUNG PERSONENBEZOGENER DATEN
Soweit wir für Verarbeitungsvorgänge personenbezogener Daten eine Einwilligung der betroffenen Person (Nutzer von <MY_SERVICES_SHORTNAME>) einholen, dient Artikel 6 Absatz 1 Buchstabe a der EU-Datenschutzgrundverordnung (DSGVO) als Rechtsgrundlage.
Für die Verarbeitung personenbezogener Daten, die zur Erfüllung eines Vertrages, dessen Vertragspartei die betroffene Person ist, erforderlich ist, dient Artikel 6 Absatz 1 Buchstabe b GDPR als Rechtsgrundlage. Dies gilt auch für Verarbeitungsvorgänge, die erforderlich sind, um Schritte vor Abschluss eines Vertrages einzuleiten.
Soweit eine Verarbeitung personenbezogener Daten zur Erfüllung einer rechtlichen Verpflichtung erforderlich ist, der unser Unternehmen unterliegt, dient Artikel 6 Absatz 1 Buchstabe c) DSGVO als Rechtsgrundlage.
Für den Fall, dass die Verarbeitung personenbezogener Daten erforderlich ist, um lebenswichtige Interessen der betroffenen Person oder einer anderen natürlichen Person zu schützen, dient Artikel 6 Absatz 1 Buchstabe d DSGVO als Rechtsgrundlage.
Ist die Verarbeitung zur Wahrung der berechtigten Interessen unseres Unternehmens oder eines Dritten erforderlich und überwiegen die Interessen, Grundrechte und Grundfreiheiten der betroffenen Person das erstgenannte Interesse nicht, so dient Artikel 6 Absatz 1 Buchstabe f) DSGVO als Rechtsgrundlage für die Verarbeitung.

3.	DATENLÖSCHUNG UND SPEICHERDAUER
Die personenbezogenen Daten der betroffenen Person werden gelöscht oder gesperrt, sobald der Zweck der Speicherung entfällt. Eine Speicherung kann darüber hinaus erfolgen, wenn dies vom europäischen oder nationalen Gesetzgeber in unionsrechtlichen Verordnungen, Gesetzen oder sonstigen Vorschriften, denen der für die Verarbeitung Verantwortliche unterliegt, vorgesehen wurde und absehbar ist. Eine Sperrung oder Löschung der Daten erfolgt auch dann, wenn eine durch die genannten Normen vorgeschriebene Speicherfrist abläuft, es sei denn, dass eine Erforderlichkeit zur weiteren Speicherung der Daten für den Abschluss oder die Erfüllung eines Vertrags besteht.


IV. BEREITSTELLUNG DER WEBSITE UND ERSTELLUNG VON LOGFILES
1.	BESCHREIBUNG UND UMFANG DER DATENVERARBEITUNG
Bei jedem Zugriff auf die Website des <MY ORGANISAION> Data Share Service (<MY_SERVICES_SHORTNAME>) erfasst unser System automatisiert Daten und Informationen vom Computersystem des zugreifenden Rechners.
Dabei werden folgende Daten erfasst:
○ Informationen über den Browsertyp und die verwendete Version
○ Das Betriebssystem des Nutzers
○ Den Internet-Service-Provider des Benutzers
○ Die pseudonymisierte IP-Adresse des Benutzers
○ Datum und Uhrzeit des Zugriffs
Die Daten werden ebenfalls in den Logfiles unseres Systems gespeichert. Diese Daten werden nicht zusammen mit anderen personenbezogenen Daten des Nutzers gespeichert.

2.	RECHTSGRUNDLAGE FÜR DIE DATENVERARBEITUNG
Rechtsgrundlage für die vorübergehende Speicherung der Daten und der Logfiles ist Artikel 6 Absatz 1 Buchstabe f) GDPR.

3.	ZWECK DER DATENVERARBEITUNG
Die vorübergehende Speicherung der IP-Adresse ist notwendig, um eine Auslieferung der Website an den Rechner des Nutzers zu ermöglichen. Dazu ist es erforderlich, dass die IP-Adresse des Nutzers für die Dauer der Sitzung gespeichert wird.
Die Speicherung der Logfiles erfolgt, um die Funktionsfähigkeit der Website zu gewährleisten. Darüber hinaus nutzen wir die Daten zur Optimierung der Website und zur Gewährleistung der Sicherheit unserer informationstechnischen Systeme. Eine Auswertung der Daten zu Marketingzwecken erfolgt in diesem Zusammenhang nicht.
Für diese Zwecke ist unser berechtigtes Interesse an der Datenverarbeitung auch nach Artikel 6 Absatz 1 Buchstabe f) DSGVO gegeben.

4.	AUFBEWAHRUNGSZEITRAUM
Die Daten werden gelöscht, sobald sie zur Erreichung des Zwecks, für den sie erhoben wurden, nicht mehr erforderlich sind. Wenn Daten erhoben werden, um das Funktionieren der Website zu ermöglichen, werden die Daten gelöscht, sobald die Sitzung beendet ist.
Bei der Speicherung von Daten in Logfiles ist dies nach spätestens sieben Tagen der Fall. Eine gewisse Speicherung über diesen Zeitraum hinaus ist möglich. In diesem Fall werden die IP-Adressen der Nutzer gelöscht oder maskiert, so dass ein Bezug zum zugreifenden Client nicht mehr möglich ist.

5.	WIDERSPRUCHSMÖGLICHKEIT UND LÖSCHUNG
Die Erfassung von Daten zur Bereitstellung der Website und die Speicherung der Daten in Logfiles ist für den Betrieb der Internetpräsenz unerlässlich. Der Nutzer hat daher keine Möglichkeit zu widersprechen.


V. VERWENDUNG VON COOKIES 
1. BESCHREIBUNG UND UMFANG DER DATENVERARBEITUNG
Nextcloud speichert nur Cookies, die für den ordnungsgemäßen Betrieb von Nextcloud erforderlich sind. Alle Cookies kommen direkt vom Nextcloud-Server, es werden keine Cookies Dritter an Ihr System gesendet.
In den Cookies werden in diesem Zusammenhang folgende Daten gespeichert und übertragen:


Cookie
Gespeicherte Daten
Lebenszeit
Sesscion Cookie
    • Sitzungs ID 
    • geheimes Token (wird zur Entschlüsselung der Sitzung auf dem Server verwendet) 24 Minuten

24 Minuten
Same-Site Cookies
Es werden keine benutzerbezogenen Daten gespeichert alle Same-Site-Cookies sind für alle Benutzer auf allen Nextcloud Instanzen für immer gleich
Für immer
Remember-me Cookie
    • Benutzer ID 
    • Ursprüngliche Sitzungs ID
    • Erinnerungs Token 
15 Tage (kann konfiguriert werden)


2. RECHTSGRUNDLAGE FÜR DIE DATENVERARBEITUNG
Die Rechtsgrundlage für die Verarbeitung personenbezogener Daten unter Verwendung von Cookies ist Artikel 6 Absatz 1 Buchstabe f) GDPR.

3. ZWECK DER DATENVERARBEITUNG
Der Zweck des Einsatzes technisch notwendiger Cookies ist es, die Nutzung der Internetseiten für die Nutzer zu vereinfachen. Einige Funktionen unserer Internetseite können ohne den Einsatz von Cookies nicht angeboten werden. Für diese ist es notwendig, dass der Browser auch nach einem Seitenwechsel wiedererkannt wird.
Für die folgenden Anwendungen benötigen wir Cookies:
    • Erfassen von Spracheinstellungen
    • Erfassen von Suchbegriffen
    • Funktionsfähigkeit der Nextcloud PHP-Anwendung 
Die durch die technisch notwendigen Cookies erhobenen Nutzungsdaten werden nicht zur Erstellung von Nutzungsprofilen verwendet.
Der Zweck des Einsatzes der Analyse-Cookies ist es, die Qualität unserer Website und ihrer Inhalte zu verbessern. 
In diesen Zwecken liegt auch unser berechtigtes Interesse an der Verarbeitung personenbezogener Daten gemäß Artikel 6 Absatz 1 Buchstabe f) GDPR.

4.	SPEICHERDAUER, WIDERSPRUCHSMÖGLICHKEIT UND ENTSORGUNG
Cookies werden auf dem Computer des Nutzers gespeichert und von dort an unsere Website übermittelt. Sie als Nutzer haben somit auch die volle Kontrolle über den Einsatz von Cookies. Durch Änderung der Einstellungen in Ihrem Internetbrowser können Sie die Übertragung von Cookies deaktivieren oder einschränken. Bereits gespeicherte Cookies können jederzeit gelöscht werden. Dies kann auch automatisch erfolgen. Bei der Deaktivierung von Cookies für unsere Website kann es sein, dass nicht mehr alle Funktionen der Website vollumfänglich genutzt werden können.


VI. RECHTE DES DATENSUBJEKTS
Wenn Ihre personenbezogenen Daten verarbeitet werden, sind Sie die betroffene Person im Sinne der DSGVO und haben die folgenden Rechte gegenüber dem Verantwortlichen:

1.	RECHT AUF AUSKUNFT
Sie können von dem für die Verarbeitung Verantwortlichen eine Bestätigung darüber verlangen, ob wir personenbezogene Daten, die Sie betreffen, verarbeitet haben oder nicht.
Für den Fall, dass eine solche Verarbeitung vorliegt, können Sie von dem für die Verarbeitung Verantwortlichen die Offenlegung der folgenden Informationen verlangen:
    • Die Zwecke, für die die personenbezogenen Daten verarbeitet werden;
    • Die Kategorien der personenbezogenen Daten, die verarbeitet werden;
    • Der Empfänger oder die Kategorien von Empfängern, denen die Sie betreffenden personenbezogenen Daten offengelegt wurden oder noch offengelegt werden;
    • Die geplante Speicherdauer der Sie betreffenden personenbezogenen Daten oder, falls es nicht möglich ist, hierzu konkrete Angaben zu machen, Kriterien für die Bestimmung der Speicherdauer;
    • Das Bestehen eines Rechts auf Berichtigung oder Löschung der Sie betreffenden personenbezogenen Daten, eines Rechts auf Einschränkung der Verarbeitung durch den Verantwortlichen oder eines Rechts auf Widerspruch gegen diese Verarbeitung;
    • Das Bestehen eines Rechts auf Beschwerde bei einer Aufsichtsbehörde;
    • Alle verfügbaren Informationen über die Herkunft der Daten, wenn die personenbezogenen Daten nicht bei der betroffenen Person erhoben wurden;
    • Das Bestehen einer automatisierten Entscheidungsfindung einschließlich Profiling gemäß Artikel 22 Absätze 1 und 4 DSGVO und - zumindest in diesen Fällen - aussagekräftige Informationen über die involvierte Logik sowie die Bedeutung und die voraussichtlichen Folgen einer solchen Verarbeitung für die betroffene Person.
Sie haben das Recht, Auskunft darüber zu verlangen, ob die Sie betreffenden personenbezogenen Daten in ein Drittland oder an eine internationale Organisation übermittelt wurden oder nicht. In diesem Zusammenhang können Sie verlangen, über die geeigneten Garantien gemäß Artikel 46 DSGVO im Zusammenhang mit der Übermittlung informiert zu werden.

2.	RECHT AUF EINSCHRÄNKUNG DER VERARBEITUNG
Sie können die Einschränkung der Verarbeitung der Sie betreffenden personenbezogenen Daten unter den folgenden Bedingungen verlangen:
    • Wenn Sie die Richtigkeit der Sie betreffenden personenbezogenen Daten während eines Zeitraums bestreiten, der es dem Verantwortlichen ermöglicht, die Richtigkeit der personenbezogenen Daten zu überprüfen;
    • Die Verarbeitung ist unrechtmäßig und Sie widersprechen der Löschung der personenbezogenen Daten und fordern stattdessen die Einschränkung der Nutzung der personenbezogenen Daten;
    • Der Verantwortliche benötigt die personenbezogenen Daten für die Zwecke der Verarbeitung nicht länger, Sie benötigen sie jedoch zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen, oder
    • sie haben gegen die Verarbeitung gemäß Artikel 21 Absatz 1 DSGVO Widerspruch eingelegt, solange noch nicht feststeht, ob die berechtigten Gründe des Verantwortlichen gegenüber Ihren Gründen überwiegen.
Wurde die Verarbeitung der Sie betreffenden personenbezogenen Daten eingeschränkt, werden diese Daten, mit Ausnahme der Speicherung, nur mit Ihrer Einwilligung oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen oder zum Schutz der Rechte einer anderen natürlichen oder juristischen Person oder aus Gründen eines wichtigen öffentlichen Interesses der Union oder eines Mitgliedstaats verarbeitet.

Wurde die Verarbeitung gemäß den oben genannten Anforderungen eingeschränkt, werden Sie von dem Verantwortlichen informiert, bevor die Einschränkung aufgehoben wird.

3.	RECHT AUF LÖSCHUNG
a) Pflicht zur Löschung
Sie können von dem für die Verarbeitung Verantwortlichen die unverzügliche Löschung der Sie betreffenden personenbezogenen Daten verlangen. Der für die Verarbeitung Verantwortliche ist verpflichtet, die Daten unverzüglich zu löschen, wenn einer der folgenden Gründe zutrifft:
    • Die Sie betreffenden personenbezogenen Daten sind im Hinblick auf die Zwecke, für die sie erhoben oder auf sonstige Weise verarbeitet wurden, nicht mehr erforderlich.
    • Sie widerrufen Ihre Einwilligung, auf die sich die Verarbeitung gemäß Artikel 6 Absatz 1 Buchstabe a oder Artikel 9 Absatz 2 Buchstabe a DSGVO stützt, und es liegt kein anderer Rechtsgrund für die Verarbeitung vor.
    • Sie legen gemäß Artikel 21 Absatz 1 DSGVO Widerspruch gegen die Verarbeitung ein, und es liegen keine vorrangigen berechtigten Gründe für die Verarbeitung vor, oder Sie legen gemäß Artikel 21 Absatz 2 DSGVO Widerspruch gegen die Verarbeitung ein.
    • Die Sie betreffenden personenbezogenen Daten wurden unrechtmäßig verarbeitet.
    • Die Sie betreffenden personenbezogenen Daten müssen zur Erfüllung einer rechtlichen Verpflichtung nach dem Unionsrecht oder dem Recht der Mitgliedstaaten, dem der Verantwortliche unterliegt, gelöscht werden.
    • Die Sie betreffenden personenbezogenen Daten wurden in Bezug auf das Angebot von Diensten der Informationsgesellschaft gemäß Artikel 8 Absatz 1 DSGVO erhoben.

b) Informationen an Dritte
Hat der für die Verarbeitung Verantwortliche die Sie betreffenden personenbezogenen Daten öffentlich gemacht und ist er gemäß Artikel 17 Absatz 1 DSGVO zur Löschung der personenbezogenen Daten verpflichtet, so trifft er unter Berücksichtigung der verfügbaren Technologie und der Implementierungskosten angemessene Maßnahmen, auch technischer Art, um für die Datenverarbeitung Verantwortliche, die die personenbezogenen Daten verarbeiten, darüber zu informieren, dass Sie als betroffene Person von diesen Verantwortlichen die Löschung aller Links zu diesen personenbezogenen Daten oder von Kopien oder Replikationen dieser personenbezogenen Daten verlangt haben.

c) Ausnahmen
Ein Recht auf Löschung besteht nicht, soweit die Verarbeitung erforderlich ist:
    • Zur Ausübung des Rechts auf freie Meinungsäußerung und Information;
    • Zur Erfüllung einer rechtlichen Verpflichtung, die dem für die Verarbeitung Verantwortlichen durch das Unionsrecht oder das Recht der Mitgliedstaaten auferlegt wurde, oder für die Wahrnehmung einer Aufgabe, die im öffentlichen Interesse liegt oder in Ausübung öffentlicher Gewalt erfolgt, die dem für die Verarbeitung Verantwortlichen übertragen wurde;
    • Aus Gründen des öffentlichen Interesses im Bereich der öffentlichen Gesundheit gemäß Artikel 9 Absatz 2 Buchstaben h und i sowie Artikel 9 Absatz 3 GDPR;
    • Zu im öffentlichen Interesse liegenden Archivzwecken, zu wissenschaftlichen oder historischen Forschungszwecken oder zu statistischen Zwecken gemäß Artikel 89 Absatz 1 DSGVO, sofern das in Abschnitt a) genannte Recht die Verwirklichung der Ziele dieser Verarbeitung wahrscheinlich unmöglich macht oder ernsthaft beeinträchtigt, oder
    • für die Begründung, Ausübung oder Verteidigung von Rechtsansprüchen.

4.	RECHT AUF INFORMATION
Haben Sie das Recht auf Berichtigung, Löschung oder Einschränkung der Verarbeitung gegenüber dem Verantwortlichen geltend gemacht, ist dieser verpflichtet, jedem Empfänger, dem die personenbezogenen Daten offengelegt wurden, diese Berichtigung oder Löschung der Daten oder Einschränkung der Verarbeitung mitzuteilen, es sei denn, dies erweist sich als unmöglich oder ist mit einem unverhältnismäßigen Aufwand verbunden.

Sie haben das Recht, von dem für die Verarbeitung Verantwortlichen über diese Empfänger informiert zu werden.

5.	RECHT AUF DATENÜBERTRAGBARKEIT
Sie haben das Recht, die Sie betreffenden personenbezogenen Daten, die Sie dem Verantwortlichen bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten. Sie haben ferner das Recht, diese Daten einem anderen Verantwortlichen ohne Behinderung durch den Verantwortlichen, dem die personenbezogenen Daten bereitgestellt wurden, zu übermitteln, sofern
    • die Verarbeitung auf einer Einwilligung gemäß Artikel 6 Absatz 1 Buchstabe a) DSGVO oder Artikel 9 Absatz 2 Buchstabe a) oder auf einem Vertrag gemäß Artikel 6 Absatz 1 Buchstabe b) DSGVO beruht und
    • die Verarbeitung mit Hilfe automatisierter Verfahren erfolgt.
Bei der Ausübung dieses Rechts haben Sie ferner das Recht, zu erwirken, dass die Sie betreffenden personenbezogenen Daten direkt von einem Verantwortlichen an einen anderen übermittelt werden, soweit dies technisch machbar ist. Die Freiheiten und Rechte anderer Personen dürfen dadurch nicht beeinträchtigt werden.

Das Recht auf Datenübertragbarkeit gilt nicht für die Verarbeitung personenbezogener Daten, die für die Wahrnehmung einer Aufgabe erforderlich ist, die im öffentlichen Interesse liegt oder in Ausübung öffentlicher Gewalt erfolgt, die dem Verantwortlichen übertragen wurde.

6.	WIDERSPRUCHSRECHT
Sie haben das Recht, aus Gründen, die sich aus Ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung Sie betreffender personenbezogener Daten, die aufgrund von Artikel 6 Absatz 1 Buchstaben e oder f DSGVO erfolgt, Widerspruch einzulegen; dies gilt auch für ein auf diese Bestimmungen gestütztes Profiling. Der Verantwortliche verarbeitet die Sie betreffenden personenbezogenen Daten nicht mehr, es sei denn, der Verantwortliche kann zwingende schutzwürdige Gründe für die Verarbeitung nachweisen, die Ihre Interessen, Rechte und Freiheiten überwiegen, oder die Verarbeitung dient der Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen. Werden die Sie betreffenden personenbezogenen Daten verarbeitet, um Direktwerbung zu betreiben, haben Sie das Recht, jederzeit Widerspruch gegen die Verarbeitung der Sie betreffenden personenbezogenen Daten zum Zwecke derartiger Werbung einzulegen; dies gilt auch für das Profiling, soweit es mit solcher Direktwerbung in Verbindung steht. Im Zusammenhang mit der Nutzung von Diensten der Informationsgesellschaft und ungeachtet der Richtlinie 2002/58/EG können Sie Ihr Widerspruchsrecht mittels automatisierter Verfahren ausüben, bei denen technische Spezifikationen verwendet werden.    

7.	RECHT AUF WIDERRUF DER DATENSCHUTZRECHTLICHEN EINWILLIGUNGSERKLÄRUNG
Sie haben das Recht, Ihre Einwilligungserklärung jederzeit zu widerrufen. Durch den Widerruf Ihrer Einwilligung wird die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung nicht berührt.

8.	AUTOMATISIERTE INDIVIDUELLE ENTSCHEIDUNGSFINDUNG, EINSCHLIESSLICH PROFILING
Sie haben das Recht, nicht einer ausschließlich auf einer automatisierten Verarbeitung - einschließlich Profiling - beruhenden Entscheidung unterworfen zu werden, die Ihnen gegenüber rechtliche Wirkung entfaltet oder Sie in ähnlicher Weise erheblich beeinträchtigt. Dies gilt nicht, wenn die Entscheidung:
    (1) für den Abschluss oder die Erfüllung eines Vertrags zwischen Ihnen und dem Verantwortlichen erforderlich ist,
    (2) aufgrund von Rechtsvorschriften der Union oder der Mitgliedstaaten, denen der für die Verarbeitung Verantwortliche unterliegt, zulässig ist und diese Rechtsvorschriften auch geeignete Maßnahmen zur Wahrung Ihrer Rechte und Freiheiten sowie Ihrer berechtigten Interessen vorsehen, oder
    (3) mit Ihrer ausdrücklichen Einwilligung erfolgt.
Diese Entscheidungen dürfen jedoch nicht auf besonderen Kategorien personenbezogener Daten gemäß Artikel 9 Absatz 1 DSGVO beruhen, es sei denn, Artikel 9 Absatz 2 Buchstaben a und g DSGVO findet Anwendung und es sind geeignete Maßnahmen zur Wahrung Ihrer Rechte und Freiheiten sowie Ihrer berechtigten Interessen vorgesehen.
In den in den Punkten (1) und (3) genannten Fällen ergreift der für die Verarbeitung Verantwortliche geeignete Maßnahmen zur Wahrung Ihrer Rechte und Freiheiten und Ihrer berechtigten Interessen, zumindest das Recht auf menschliches Eingreifen seitens des für die Verarbeitung Verantwortlichen, auf Darlegung des eigenen Standpunkts und auf Anfechtung der Entscheidung.

9.	RECHT AUF BESCHWERDE BEI EINER AUFSICHTSBEHÖRDE
Unbeschadet eines anderweitigen verwaltungsrechtlichen oder gerichtlichen Rechtsbehelfs steht Ihnen das Recht auf Beschwerde bei einer Aufsichtsbehörde, insbesondere in dem Mitgliedstaat ihres Aufenthaltsorts, ihres Arbeitsplatzes oder des Orts des mutmaßlichen Verstoßes, zu, wenn Sie der Ansicht sind, dass die Verarbeitung der Sie betreffenden personenbezogenen Daten gegen die DSGVO verstößt.

Die Aufsichtsbehörde, bei der die Beschwerde eingereicht wurde, wird den Beschwerdeführer über den Fortgang und das Ergebnis der Beschwerde einschließlich der Möglichkeit eines gerichtlichen Rechtsbehelfs nach Artikel 78 DSGVO unterrichten.

Die für das Deutsche Zentrum für Diabetesforschung (<MY ORGANISAION>) zuständige Aufsichtsbehörde ist der Bundesbeauftragte für den Datenschutz und die Informationsfreiheit <Bundesbeauftragter für den Datenschutz und die Informationsfreiheit>, Husarenstr. 30, 53117 Bonn, Deutschland, Tel.: +49 228-997799-0, E-Mail: poststelle@bfdi.bund.de.