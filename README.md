# DZD DataSharingService

Author: Tim Bleimehl  
Status: Work in Process  
confidentiality: None - OpenSource(MIT-Licence)  

This document describes how to setup a Nextcloud as data sharing service according to [DZD Whitepaper "Sharing Pseudonymized Medical Data Via Nextcloud Securely"](https://wiki.connect.dzd-ev.de/en/public/share-medical-data-concept)

- [DZD DataSharingService](#dzd-datasharingservice)
  - [Target Audience](#target-audience)
  - [Goals](#goals)
  - [Scope](#scope)
  - [Baseline](#baseline)
    - [Requirements](#requirements)
    - [Assets](#assets)
    - [Article Representation](#article-representation)
  - [Create Infrastructure](#create-infrastructure)
    - [Create Reverse Proxy](#create-reverse-proxy)
    - [Setup storage](#setup-storage)
      - [Create a storage bucket](#create-a-storage-bucket)
      - [Access the bucket](#access-the-bucket)
    - [Create the Nextcloud instance](#create-the-nextcloud-instance)
  - [Nextcloud configuration](#nextcloud-configuration)
    - [Mail Server Configuration](#mail-server-configuration)
    - [Storage Configuration](#storage-configuration)
    - [Booting Nextcloud](#booting-nextcloud)
    - [Nextcloud Apps](#nextcloud-apps)
      - [Disable All Apps](#disable-all-apps)
      - [Enable Necessary Apps](#enable-necessary-apps)
    - [File Retention](#file-retention)
    - [Activity log retention](#activity-log-retention)
    - [Enable File Encryption](#enable-file-encryption)
    - [Set Cron Job](#set-cron-job)
    - [Account Retention](#account-retention)
    - [Restrict Sharing To Group Members](#restrict-sharing-to-group-members)
    - [Adapt User registration](#adapt-user-registration)
    - [Terms Of Conditions](#terms-of-conditions)
  - [Tests](#tests)
    - [Testing Mail SMTP Settings](#testing-mail-smtp-settings)
  - [Hardening](#hardening)
    - [MinIO](#minio)
  - [Tips \& Troubleshooting](#tips--troubleshooting)
    - [Logging](#logging)
    - [Local test instance](#local-test-instance)
  - [Maintenance](#maintenance)
    - [Update](#update)

## Target Audience

Local IT-administrators/-responsibles of federated organizations (like the DZD or any other DZG) that need a quick and easy solution to enable their user to share data between otherwise fairly unconnected sites.

## Goals

The goal is to create a datasharing plattform with following aspirations:
* A focus on a small footprint in terms of data protection regulations from an IT operations perspective.
* Seperate application and storage environment to create a reasonable secure environment from an IT operations perspective
* Use "off the shelf" open source components to enable IT-Administrators to easily clone our concept
* Users should have a reasonable easy experience to upload and share data with their peers. Any processes that can be automated for the users should be (e.g. Automatic file deletion to follow data protection regulations)

## Scope

We cover the setup and configuration of the following components: 
* A [Nextcloud](https://nextcloud.com/) instance from scratch and stripped down to the essential needs
* A [Treafik](https://doc.traefik.io/traefik/) instance for reverse proxying, certificate management, SSL termination and routing
* A [MinIO](https://min.io/) instance as a S3 storage backend.

**Explanatory note**: There are many different ways to achieve a setup we are describing in this article. This one focuses on modularity, common open source components and reasonable simplicity.


## Baseline

### Requirements

* Two hosts/machines able run Docker or Podman containers (in this document we will use `docker` and `docker-compose` on a linux host with `bash` )
  * Both machines need a public IP
  * Both machines need ports 80 and 443 open to the public
  * Specifications for the hosts:
    * at least 2 Cores and 8 GB Memory for a small test setup. 
    * a system storage/hdd of minimum 60GB. SSD or NVME storage is strongly advised to improve the user experience.
    * One of the machines, serving as the storage server, should have a larger storage according to your needs and possibilities. Perfomance for this storage is not as important.
* Two public (sub-)domains showing on each targeting one of the machines (`storage.example.org` and `share.example.org` as an example in this article)
* Storage (S3, SFTP, SMB/CIFS, WebDAV, OpenStack. In this article we will use a basic S3 storage)
* Some basic terminal skills and the ability to edit/create files

If your IT department demands a reasoning for opening the two ports:
* **Ports 80** is needed for [ACME https challenge](https://letsencrypt.org/docs/challenge-types/#http-01-challenge) to prove that you are controlling the host and are thustworthy to get a SSL certificate. That will enable encrypted commuication over port 443 in the first place
* **Port 443** is our secure endpoint our web apps and APIs will communicate through via `https`.


Lets have an overview of our baseline setup structure:

![alt text](diagrams/01_baseline.png "Baseline")

### Assets

* We will assume a Debian based Linux host with `bash` in this article. Commands and solutions may differ if you use a different environment. We assume your user has permission to run `docker` commands. If not, you may have to put `sudo` in front of all `docker` commands

* The base layer for our project will be the [official Nextcloud Container Image from Dockerhub](https://hub.docker.com/_/nextcloud) in version `25`

* As a reverse proxy and ssl certificate handler we use Traefik. We will use the [official container image from Dockerhub](https://hub.docker.com/_/traefik) in version `v2.9`

* To simulate a S3 storage in our example we will use a simple https://hub.docker.com/r/minio/minio/ setup.

### Article Representation

In this article we will provide bash commands. You will identify them as boxed text in monospace fonts. Example:

```bash
echo "hello world"
```

If we referece files that need to be edited or created, we wont suggest an editor. We will just use the `touch` command to indicate a file creation and provide the content in the next lines as a box led by the filename. Example:

```bash
touch /tmp/hello.yaml
```

> **/tmp/hello.yaml**
> ```yaml
> message: hello
> target: world
> ```

As this article build up on a setup with two hosts we will also always reference the machines name to run these commands on with

> @`storage01`-Machine

or

> @`share01`-Machine


## Create Infrastructure

The next subchapters will cover the set up of all components that will build our sharing plattform.

### Create Reverse Proxy

> In this chapter we will setup a reverse proxy to manage our connections from external clients and certificates to encrypt these connections. In your case the setup and requirements may be different. If you allready have SSL certificates and you are able to manage subdomains you can skip this step.

In the past obtaining certificates, to be able to provide a SSL secured connection for your services, was a complex and relative costly task. Nowadays this is almost a non-brainer. This is thanks to the non-profit certificate authority [`https://letsencrypt.org`](https://letsencrypt.org), their automatisation API [ACME](https://de.wikipedia.org/wiki/Automatic_Certificate_Management_Environment) and third party tools like [traefik](https://doc.traefik.io/traefik/).

Besides automated certification management, we will use Traefik also as a [reverse proxy](https://de.wikipedia.org/wiki/Reverse_Proxy).
This gives us an easy way manage our subdomains. In our case we will have multiple services/apps running on our host: The Nextcloud app with its subcomponents, our storage and maybe other webapplication we may add later.  
As we need to encrypt our connections on both machines, we need to start a Traefik instance on each of our hosts.

Lets start with creating a docker-compose file for our Traefik instances

> @`share01`-Machine **&**  @`storage01`-Machine

```bash
mkdir -p /srv/docker-composes/traefik && cd /srv/docker-composes/traefik
```

> @`share01`-Machine **&**  @`storage01`-Machine
```bash
touch /srv/docker-composes/traefik/docker-compose.yaml
```

> @`share01`-Machine **&**  @`storage01`-Machine

> **/srv/docker-composes/traefik/docker-compose.yaml**
> ```yaml
>version: "3.7"
>services:
>  traefik:
>    image: "traefik:2.9"
>    container_name: traefik
>    restart: always
>    command:
>      - --log.level=INFO
>      - --entrypoints.web.address=:80
>      - --entrypoints.webssl.address=:443
>      - --providers.docker=true
>      - --certificatesresolvers.letsencrypt-resolver.acme.caserver=https://acme-v02.api.letsencrypt.org/directory
>      - --certificatesresolvers.letsencrypt-resolver.acme.email=YOUR.MAIL@EXAMPLE.ORG
>      - --certificatesresolvers.letsencrypt-resolver.acme.storage=/ssl/json/acme.json
>      - --certificatesresolvers.letsencrypt-resolver.acme.httpChallenge.entryPoint=web
>    ports:
>      - 80:80
>      - 443:443
>    volumes:
>      # Treafik needs to read the docker socket to be able to see other containers that are demanding to be proxied by traefik
>      - /var/run/docker.sock:/var/run/docker.sock:ro
>      - ./data/:/ssl/json
>    labels:
>      # global redirect to https
>      - "traefik.http.routers.http-catchall.rule=hostregexp(`{host:.+}`)"
>      - "traefik.http.routers.http-catchall.entrypoints=web"
>      - "traefik.http.routers.http-catchall.middlewares=redirect-to-https"
>      # middleware redirect
>      - "traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https"
>```

Do not forget to provide your mail address at `--certificatesresolvers.letsencrypt-resolver.acme.email` to register as the administrator/owner for your certificates.


Lets start our Traefik instances: 

> @`share01`-Machine **&**  @`storage01`-Machine
```bash
docker-compose up -d
```

Lets have an overview of our current setup structure:

![alt text](diagrams/02_traefik.png "Baseline")
### Setup storage


> In this chapter we will setup a MinIO instance so simulate a remote S3 storage. In your case the setup and requirements will most likely be totaly different. If you allready have a storage you can skip this step.

Lets create a subdirectory for our "storage server" and create a docker-compose file in it:

> @`storage01`-machine

```bash
mkdir -p /srv/docker-composes/minio && cd /srv/docker-composes/minio
```


> @`storage01`-machine
```bash
touch docker-compose.yaml
```

> @`storage01`-machine

> **docker-compose.yaml**
> ```yaml
> version: '3.7'
> services:
>   minio:
>     container_name: minio1
>     command: server /data --console-address ":9001"
>     environment:
>       - MINIO_ROOT_USER=admin
>       - MINIO_ROOT_PASSWORD=TOPSECRET_ADMIN
>     image: quay.io/minio/minio:latest
>     ports:
>       - 'localhost:9000:9000'
>       - 'localhost:9001:9001'
>     volumes:
>       - ./data:/data
>     restart: unless-stopped
>     labels:
>      - "traefik.enable=true"
>      - "traefik.http.services.srv-minio1.loadbalancer.server.port=9000"
>      - "traefik.http.routers.rt-minio1.service=srv-minio1"
>      - "traefik.http.routers.rt-minio1.entrypoints=webssl"
>      - "traefik.http.routers.rt-minio1.rule=Host(`storage.example.org`)"
>      - "traefik.http.routers.rt-minio1.tls=true"
>      - "traefik.http.routers.rt-minio1.tls.certResolver=letsencrypt-resolver"
>
>      - "traefik.http.services.srv-minio1-api.loadbalancer.server.port=9001"
>      - "traefik.http.routers.rt-minio1-api.service=srv-minio1-api"
>      - "traefik.http.routers.rt-minio1-api.entrypoints=webssl"
>      - "traefik.http.routers.rt-minio1-api.rule=Host(`storage-api.example.org`)"
>      - "traefik.http.routers.rt-minio1.api.tls=true"
>      - "traefik.http.routers.rt-minio1-api.tls.certResolver=letsencrypt-resolver"
>     healthcheck:
>       test: ["CMD", "curl", "-f", "http://localhost:9000/minio/index.html", "||", "exit 1"]
>       interval: 30s
>       timeout: 3s
>       retries: 3
> ```


> @`storage01`-machine
```bash
docker-compose up -d
```

Our storage will now be publicly reachable, behind an authorization login, at the URLs `https://storage.example.org` and `https://storage-api.example.org`. Visit [`https://storage.example.org`](https://storage.example.org) to verifiy the function of our storage interface.

#### Create a storage bucket

Lets create a new s3 bucket to store our Nextcloud data later

> @`storage01`-machine

```bash
docker run --rm \
--entrypoint sh minio/mc -c \
"mc config host add minio1 http://minio1:9000 admin TOPSECRET_ADMIN && \
mc mb minio1/nextcloud-bucket"
```

#### Access the bucket

Our new storage bucket API is localy available at `http://localhost:9000/buckets/nextcloud-bucket`
You can also use the MinIO WebClient to check out your new bucket at `http://localhost:9001/buckets/nextcloud-bucket`

> In this simple example we will use the root user `admin` and root password `TOPSECRET_ADMIN` to access the bucket. In a productive enironment you want to create an extra MinIO user with minimal access policies.


Lets have an overview of our current setup structure:

![alt text](diagrams/03_storage.png "storage")

### Create the Nextcloud instance

Lets wrap thing up first in simple terms: Until now we created the base infrastructure for our Nextcloud Instance
* A seperated place to store our users files that they want to share
* A "gateway" to welcome our users that will route them to the applications they want to talk to and that takes care of encrypted communication.

In the next step we want deploy and setup the applicaton that will enable the sharing of the data.

The Nextcloud deployment then again will consists of multiple components:
* A Redis instance. A very fast small database used as a volatile cache to improve perfomance
* A Postgres SQL Database. A classic relational Database to permanently store all metadata like usernames, configurations, shares, etc
* A webserver running the Nextcloud app. The PHP Application executed by a Webserver itself running the busines logic and combining all our building blocks

> @`share01`-machine
```bash
mkdir -p /srv/docker-composes/nextcloud && cd /srv/docker-composes/nextcloud
```

> @`share01`-machine
```bash
touch /srv/docker-composes/nextcloud/docker-compose.yaml
```
> @`share01`-machine

> /srv/docker-composes/nextcloud/docker-compose.yaml
> ```yaml
> version: "3.7"
> services:
>   redis-cache:
>     image: redis:alpine
>     container_name: redis1
>     restart: always
>     labels:
>       - traefik.enable=false
>   postgres-database:
>     image: postgres:12
>     container_name: postgres1
>     restart: always
>     environment:
>       - POSTGRES_PASSWORD=TOPSECRET_ADMIN_DB
>       - POSTGRES_DB=nextcloud
>       - POSTGRES_USER=nextcloud
>       - POSTGRES_INITDB_ARGS=--lc-collate C --lc-ctype C --encoding UTF8
>     volumes:
>       - ./data/db:/var/lib/postgresql/data
>     healthcheck:
>       test: "pg_isready -h localhost -p 5432 -q -U postgres"
>       interval: 3s
>       timeout: 5s
>       retries: 5
>     labels:
>       - traefik.enable=false
>   nextcloud:
>     image: nextcloud:25
>     restart: always
>     container_name: nextcloud1
>     depends_on:
>       postgres-database:
>         condition: service_healthy
>     volumes:
>       - ./data/nextcloud:/var/www/html
>       - ./config/nextcloud/app:/var/www/html/config
>       - ./log/nextcloud:/log
>       - ./config/nextcloud/php/z-php-custom-setting.php.ini:/usr/local/etc/php/conf.d/z-php-custom-setting.php.ini
>     environment:
>       - POSTGRES_HOST=postgres1
>       - POSTGRES_DB=nextcloud
>       - POSTGRES_USER=nextcloud
>       - POSTGRES_PASSWORD=TOPSECRET_ADMIN_DB
>       - NEXTCLOUD_ADMIN_USER=admin
>       - NEXTCLOUD_ADMIN_PASSWORD=TOPSECRET_ADMIN
>       - REDIS_HOST=redis1
>       - NEXTCLOUD_TRUSTED_DOMAINS=share.example.org nextcloud1
>       - TRUSTED_PROXIES=<YOUR.PUBLIC.IP.000>
>       - OVERWRITEPROTOCOL=https
>       - PHP_MEMORY_LIMIT=1024M
>       - PHP_UPLOAD_LIMIT=64G
>     labels:
>       # ### Traefik integration ###
>       - "traefik.enable=true"
>       - "traefik.http.services.srv-nextcloud.loadbalancer.server.port=80"
>       - "traefik.http.routers.rt-nextcloud.service=srv-nextcloud"
>       - "traefik.http.routers.rt-nextcloud.entrypoints=webssl"
>       - "traefik.http.routers.rt-nextcloud.rule=Host(`share.example.org`)"
>       - "traefik.http.routers.rt-nextcloud.tls=true"
>       - "traefik.http.routers.rt-nextcloud.tls.certResolver=letsencrypt-resolver"
>       # Some Traefik middelware
>       - "traefik.http.routers.rt-nextcloud.middlewares=mw-nextcloud,mw-nextcloud_redirect"
>       - "traefik.http.middlewares.mw-nextcloud.headers.stsSeconds=155520011"
>       - "traefik.http.middlewares.mw-nextcloud.headers.stsIncludeSubdomains=true"
>       - "traefik.http.middlewares.mw-nextcloud.headers.stsPreload=true"
>       - "traefik.http.middlewares.mw-nextcloud.headers.contentTypeNosniff=true"
>       - "traefik.http.middlewares.mw-nextcloud.headers.browserXSSFilter=true"
>       - "traefik.http.middlewares.mw-nextcloud_redirect.redirectregex.permanent=true"
>       - "traefik.http.middlewares.mw-nextcloud_redirect.redirectregex.regex=/.well-known/(card|cal)dav"
>       - "traefik.http.middlewares.mw-nextcloud_redirect.redirectregex.replacement=/remote.php/dav/"
>     healthcheck:
>       test: [ "CMD", "curl", "--fail", "http://localhost" ]
>       interval: 1m
>       timeout: 10s
>       retries: 3
>   cron:
>     image: nextcloud:25-fpm
>     restart: always
>     volumes:
>       - ./data/nextcloud:/var/www/html
>     entrypoint: /cron.sh
>     depends_on:
>       - nextcloud
>       - redis-cache
> ```


We now have completed our basic infrastructure. All components are there. Lets have an overview:

![alt text](diagrams/04_complete.png "complete")

We wont boot the Nextcloud instance as we still need to configure some environment specific details...
## Nextcloud configuration

### Mail Server Configuration

A functional email configuration is very important for your Nextcloud instance. This enables user registration, user password self management and user notifications.
We can pre configure the mail server coordinates with environment variables via our docker-compose file.
Alternatively you can also skip this step and do it via the WebClient Admin Area later (See https://docs.nextcloud.com/server/stable/admin_manual/configuration_server/email_configuration.html#configuring-an-smtp-server ). 

A mail configuration is highly individual depending on your environment. In this example we will take a quick prototyping approach and use a free Gmail account as an visual example. **This setup is absolutly not recommended for productive use.** Gmail will kick you out if you send too much mails automated from their servers. Also it will be easy for hackers [phish](https://en.wikipedia.org/wiki/Phishing) your users with similar Gmail adresses. A mail from an owned domain is much more trustwhorty and harder to fake.

How to prepare your Gmail account to be used as SMTP server without leaving your personal password in a config file you can read here https://hotter.io/docs/email-accounts/app-password-Gmail/

> @`share01`-machine

> /srv/docker-composes/nextcloud/docker-compose.yaml  
> ```yaml
> version: '3.7'
> services:
> ```
> `[...]`
> ```yaml
>   nextcloud:
> ```
> `[...]`
> ```yaml
>     environment:
> ```
> `[...]`
> ```yaml
>       # ### Mail Server ###
>       - SMTP_HOST=smtp.google.com # (not set by default): The hostname of the SMTP server.
>       - SMTP_SECURE=ssl #(empty by default): Set to ssl to use SSL, or tls to use STARTTLS.
>       - SMTP_PORT=465 #(default: 465 for SSL and 25 for non-secure connections): Optional port for the SMTP > connection. Use 587 for an alternative port for STARTTLS.
>       - SMTP_AUTHTYPE=LOGIN #(default: LOGIN): The method used for authentication. Use PLAIN if no > authentication is required.
>       - SMTP_NAME=my_nextcloud_mail@gmail.com #(empty by default): The username for the authentication.
>       - SMTP_PASSWORD=TOPSECRET_ADMIN_GMAIL_APP_PASSWORD #(empty by default): The password for the authentication.
>       - MAIL_FROM_ADDRES=my_nextcloud_mail #(not set by default): Use this address for the 'from' field in the > emails sent by Nextcloud.
>       - MAIL_DOMAIN=gmail.com #(not set by default): Set a different domain for the emails than the domain > where Nextcloud is installed.
>       # ### External S3 Storage ###
> ```
> `[...]`

For details on all other mail options see https://docs.nextcloud.com/server/25/admin_manual/configuration_server/email_configuration.html#setting-mail-server-parameters-in-config-php

### Storage Configuration

As mentioned and configured in this article we exemplary use a MinIO instance to show a connection to a S3 Storage. Which kind of storage you attach to your nextcloud and how you do that is highly dependent upon your needs, requirement and the existing environment.

https://docs.nextcloud.com/server/25/admin_manual/configuration_files/primary_storage.html#simple-storage-service-s3

If your storage is another host (which is recommended) you can not use the local docker hostnames.  
In our example the environment variables would look like this:

> @`share01`-machine

> /srv/docker-composes/nextcloud/docker-compose.yaml  
> ```yaml
> version: '3.7'
> services:
> ```
> `[...]`
> ```yaml
>   nextcloud:
> ```
> `[...]`
> ```yaml
>     environment:
> ```
> `[...]`
> ```yaml
>       # ### External S3 Storage ###
>       - OBJECTSTORE_S3_HOST=storage.example.org
>       - OBJECTSTORE_S3_BUCKET=nextcloud-bucket
>       - OBJECTSTORE_S3_KEY=admin
>       - OBJECTSTORE_S3_SECRET=TOPSECRET_ADMIN
>       - OBJECTSTORE_S3_PORT=9000
>       - OBJECTSTORE_S3_SSL=true
>       - OBJECTSTORE_S3_REGION=
>       - OBJECTSTORE_S3_USEPATH_STYLE=true #(default: false): Not required for AWS S3
>       - OBJECTSTORE_S3_LEGACYAUTH=false #Not required for AWS S3
>       #- OBJECTSTORE_S3_OBJECT_PREFIX= #(default: urn:oid:): Prefix to prepend to the fileid
>       - OBJECTSTORE_S3_AUTOCREATE=false #Create the container if it does not exist
> ```
> `[...]`

### Booting Nextcloud


> @`share01`-machine
```bash
cd /srv/docker-composes/nextcloud/
```

> @`share01`-machine
```bash
docker-compose up -d
```

### Nextcloud Apps

Our final goal is to have a Nextcloud instance that is as minimalistic as possible. First of all we want to narrow down the range of capabilities the users can perform. This is a special purpose Nextcloud. Users should, as far as possible, not have too many possibility to make mistakes. 

Secondly this will lessen the probability of unwanted events like unecessary logging, communication and (worst case) attacks on unpatched/unknown security exploits. Less complexity means less data and less attack vectors.

By default Nextcloud tries to serve many roles. It wants to be a federated file sync tool, a calender provider, a photo managment app, a contact manager, and much more. Essentially we want 2 things: single instance file sharing and activity logging. And we will need some more smaller Nextcloud apps to achieve some side goals like automatic file retention.

#### Disable All Apps

To provide a common baseline we first disable all apps in our Nextcloud

Lets temporary install some basic tools for our task in the Nextcloud container.

> @`share01`-machine
```bash
docker exec nextcloud1 /bin/sh -c "apt update && apt install -y sudo jq"
```

Generate a list of all currently enabled Nextcloud apps and save their names into a variable called `ALL_APP_NAMES`

> @`share01`-machine
```bash
ALL_APP_NAMES=$(docker exec nextcloud1 /bin/sh -c "sudo -E -u www-data php occ app:list --output=json | jq -r '.[\"enabled\"] | keys | join(\",\")'")
```

Parse the string list into a bash array named `ALL_APP_NAMES_ARRAY`

> @`share01`-machine
```bash
IFS=', ' read -r -a ALL_APP_NAMES_ARRAY <<< "$ALL_APP_NAMES"
```

Pass the array to the disable app command of our Nextcloud

> @`share01`-machine
```bash
for APP_NAME in ${ALL_APP_NAMES_ARRAY[@]}; do docker exec nextcloud1 /bin/sh -c "sudo -E -u www-data php occ app:disable ${APP_NAME}" ;done
```


**hint**: If you experience any problems with these commands or you want to disable apps manually you can also disable the apps via the Web UI in the Apps settings ( `https://share.example.org/settings/apps` in our example)

#### Enable Necessary Apps

Now we only enable the apps we want to use for our sharing plattform

> @`share01`-machine
```bash
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ app:enable -f {activity,files_downloadactivity,files_trackdownloads,files_rightclick,files_automatedtagging,files_retention,files_sharing,systemtags,encryption,logreader,serverinfo,password_policy,privacy,registration,terms_of_service,theming,drop_account,user_retention,end_to_end_encryption}"
```

Lets have a short look on each app and justify why we enable/install it

* [`activity`](https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/activity_configuration.html) - Basic logging app to be able to retrace any sharing activities. 
* [`files_downloadactivity`](https://apps.nextcloud.com/apps/files_downloadactivity) - We want to track each download to be able to retrace who downloaded our data in case of a data protection regulations breach.
* [`files_trackdownloads`](https://apps.nextcloud.com/apps/files_trackdownloads) - Same as `files_downloadactivity` but visible to all admins and not just the users.
* [`files_rightclick`](https://apps.nextcloud.com/apps/files_rightclick) - Just a quality of life app. Will enable to access the file sharing menu via rightclick
* [`files_automatedtagging`](https://apps.nextcloud.com/apps/files_automatedtagging) - We want to tag all files automated to track their lifetime. That will enable us to auto delete them after a certain timeperiod. https://docs.nextcloud.com/server/25/admin_manual/file_workflows/automated_tagging.html
* [`files_retention`](https://apps.nextcloud.com/apps/files_retention) - This app will delete the files that are tagged by `files_automatedtagging`. https://docs.nextcloud.com/server/25/admin_manual/file_workflows/retention.html
* [`files_sharing`](https://docs.nextcloud.com/server/latest/user_manual/en/files/sharing.html) - The Nextcloud native sharing app. This enables to share files with peers in the first place. Otherwise our NExtcloud Instance would just be filesink.
* `systemtags` - Collaborative tags - Enable tags that are available for all users. https://docs.nextcloud.com/server/25/admin_manual/file_workflows/retention.html#public-collaborative-tag
* `encryption` - enables encryption of every user file that is stored to our Nextcloud instance. this will prevent any unwanted access via the storage server. https://docs.nextcloud.com/server/19/user_manual/files/encrypting_files.html
* [`logreader`](https://github.com/nextcloud/logreader) - Let you read the logs via the webinterface. Helps debugging any issues.
* [`serverinfo`](https://github.com/nextcloud/serverinfo) - Show you some data about memory,storage and cpu usage. Can be helpful for debuging
* [`password_policy`](https://github.com/nextcloud/password_policy) - Lets you decide how long and complex the user passwords have to be
* [`privacy`](https://github.com/nextcloud/privacy) - The privacy center shows you where your data is stored and who can access it
* [`registration`](https://apps.nextcloud.com/apps/registration) - Enables the users to register for your sharing service.
* [`terms_of_service`](https://apps.nextcloud.com/apps/terms_of_service) - In order to be compliant with your local terms/laws of data protection your users need to agree to a "terms of service"-text
* [`theming`](https://apps.nextcloud.com/apps/theming) - In order to show the affiliation of your service to your organization and improve trust, acceptability and communicate professionalism it helps to brand your service.
* [`drop_account`](https://apps.nextcloud.com/apps/drop_account) - In order to be compliant with your local terms/laws of data protection it maybe a must that users are able to delete their data.
* [`user_retention`](https://apps.nextcloud.com/apps/user_retention) - In order to be compliant with your local terms/laws of data protection it maybe a must that inactive users will be deleted automated
* [`end_to_end_encryption`](https://apps.nextcloud.com/apps/end_to_end_encryption) - This enables user to share data end to end encrypted via the Nextcloud client. This can be useful for certain use cases


### File Retention

Our goal in this chapter to create an automated background job (aka workflow) that will delete files 30 days after they have been uploaded or changed. This will prevent any forgoten files that accidentally breach a data protection rule in terms of retention times.

If not allready done in the [`Nextcloud Apps`](#nextcloud-apps) section, we need to temporary install some basic tools in our Nextcloud container.

> @`share01`-machine
```bash
docker exec nextcloud1 /bin/sh -c "apt update && apt install -y sudo jq"
```

Lets create an invisble file tag named `expire_file` and save its ID to a bash variable named `TAG_ID` 

> @`share01`-machine

```bash
TAG_ID=$(docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ tag:add expire_file invisible --output=json | jq -r '.[\"id\"]'")
```

Next we will create a workflow that will attach our new tag `expire_file` to every file that will be uploaded by a non admin.


> @`share01`-machine
```bash
docker exec nextcloud1 /bin/bash -c "curl -v -L -X POST 'http://localhost:80/ocs/v2.php/apps/workflowengine/api/v1/workflows/global?format=json' \
  -u admin:TOPSECRET_ADMIN \
  -H 'OCS-APIRequest: true' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Origin: http://localhost:80' \
  --data-raw '{\"class\":\"OCA\\\FilesAutomatedTagging\\\Operation\",\"entity\":\"OCA\\\WorkflowEngine\\\Entity\\\File\",\"events\":[],\"name\":\"\",\"checks\":[{\"class\":\"OCA\\\WorkflowEngine\\\Check\\\UserGroupMembership\",\"operator\":\"!is\",\"value\":\"admin\",\"invalid\":false}],\"operation\":\"${TAG_ID}\",\"valid\":true}'\
"
```

Now that our files will all be tagged, we can now tell Nextcloud to delete all taged files after 30 days.

> ~~ToDo: This api command has issues. https://github.com/nextcloud/files_retention/issues/254 use [this hotfix](file_retention_hotfix.sh) before running the below command while the fix is released~~
> 
> This command just works now with current release of file_retention.

> @`share01`-machine
```bash
docker exec nextcloud1 /bin/bash -c "curl -v -L -X POST 'http://localhost:80/ocs/v2.php/apps/files_retention/api/v1/retentions' \
  -u admin:TOPSECRET_ADMIN \
  -H 'OCS-APIRequest: true' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Origin: http://localhost:80' \
  --data-raw '{\"tagid\":${TAG_ID},\"timeamount\":30,\"timeunit\":0,\"timeafter\":1}'
"
```

Alternativly you can create the workflow with it tags via the WebUI at https://share.example.org/settings/admin#systemtags and https://share.example.org/settings/admin/workflow#configured-flows 

Our result at https://share.example.org/settings/admin/workflow should look like this

![Workflow screenshot](images/Workflow.png "Workflow")

### Activity log retention

Same as for files; we dont want to save our activity log forever. In our example we want to delete the activity logs after a year

https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/activity_configuration.html#configuring-your-nextcloud-for-the-activity-app

```bash
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ config:system:set activity_expire_days --value=365 --type=integer"
```
https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/activity_configuration.html#activities-in-groupfolders-or-external-storages

Also, as our storage is external, we want to enable activity logs for external storage.

```bash
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ config:system:set activity_use_cached_mountpoints --value=true --type=boolean"
```


### Enable File Encryption

We want to encrypt all files before storing it on our storage server. This ensures two seperated environments from a IT operations standpoint. The storage administrator will not be able to read any informations from the files in our Nextcloud.

```bash
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ encryption:enable"
```

For details about the command see https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/encryption_configuration.html#occ-encryption-commands

### Set Cron Job

Nextcloud needs a periodic trigger to process background jobs.
Background tasks are triggered by our `cron` docker-compose service.

We just need to say the Nextcloud instance that backgroundjobs are allready taken care of.

```bash
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ background:cron"
```

For details on this topic see https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/background_jobs_configuration.html#cron


### Account Retention

Inactive user accounts should be removed after a year of not logging into the system. This prevents any unwanted data hoarding.

> @`share01`-machine
```
curl 'http://localhost:8082/ocs/v2.php/apps/provisioning_api/api/v1/config/apps/user_retention/user_days' -X POST 
--data-raw 'value=356'
```


### Restrict Sharing To Group Members

To prevent any unwanted/accidental data sharing we set the following rules:

* Users should only be able to share data in their group. This prevents sharing by accident with "strangers".
* User should not be allowed to share by public link. This prevents any uncontrolled passing to third parties.
* By default any shared file should not alterable
* User should not be able to use federated shares to other servers. We can not controll what happens to the data on other Nextcloud servers.

To enforce these rules we run following commands

> @`share01`-machine
```bash
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_enabled --value=yes"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_allow_links --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_allow_resharing --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_allow_group_sharing --value=yes"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_only_share_with_group_members --value=yes"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_exclude_groups --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_allow_share_dialog_user_enumeration --value=yes"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_restrict_user_enumeration_to_group --value=yes"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_restrict_user_enumeration_to_phone --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_restrict_user_enumeration_full_match --value=yes"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_default_permissions --value=1"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core shareapi_default_permission_canupdate --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core outgoing_server2server_share_enabled --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core outgoing_server2server_group_share_enabled --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core incoming_server2server_share_enabled --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core incoming_server2server_group_share_enabled --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set core lookupServerUploadEnabled --value=no"
```


### Adapt User registration

> if you have a user directory (-service) in your organization like LDAP or openID Connect and you are able to connect it, you can skip this step. With a proper user directory (e.g. LDAP, MS Active Directory) you can automate the process of authentication and authorization of your users. See https://docs.nextcloud.com/server/latest/admin_manual/configuration_user/user_auth_ldap.html for details.


The goal in this chapter is to prevent unauthenticated user (aka "strangers") to enter/user our system. But new user should also be able register themselves. To achieve this we want the registration to be open but every accounts needs to be unlocked by an admin after registration. This way an admin can verify the affiliation by the email address for example.

Run these commands to enable and configure the registraation module.

> @`share01`-machine
```bash
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration enabled --value=yes"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration show_phone --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration enforce_phone --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration domains_is_blocklist --value=''"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration show_domains --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration email_is_optional --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration disable_email_verification --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration email_is_login --value=no"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration show_fullname --value=yes"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration additional_hint --value='Please provide your email and full name that we can validate your affiliation.'"
docker exec nextcloud1 /bin/bash -c "sudo -E -u www-data php occ  config:app:set registration admin_approval_required --value=yes"
```


### Terms Of Conditions

It is important to provide a proper terms of conditions to clear up rights, duties, intented purposes and limitations for the users. 

At the moment there is no way to setup the terms of service automated via a command or script. You have to do it in the WebUI.  
Go to http://localhost:8082/settings/admin/terms_of_service and fill out the form.  

In future there may be way a setup the terms programmaticly. (I will update the script then) https://github.com/nextcloud/terms_of_service/issues/258  

> We provide a german template for use in Germany [here](templates/conditions-text/german.txt)

>If you created a terms of conditions for another country or language, we would be happy to include it here (contact: tim.bleimehl@helmholtz-muenchen.de)


## Tests


*Todo*

### Testing Mail SMTP Settings

*Todo*


## Hardening

*Todo*
### MinIO

*Todo*

Close uneseccary ports 9000,9001


## Tips & Troubleshooting


*Todo* 


### Logging

Set loglevel to debug

https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/logging_configuration.html

```bash
docker exec nextcloud1 /bin/bash -c "sudo -u www-data php occ config:system:set loglevel --value=0"
```

### Local test instance

To create a local test, debuging instance or to just dabble in the whole concept, we provide a docker-compose file [here](demo-setup/all-in-one-local-demo/docker-compose.yaml)

To use it just clone the repo and start the compose file

```bash
git clone ssh://git@git.connect.dzd-ev.de:22022/dzdtools/dzddatasharingservice.git
```
```bash
cd dzddatasharingservice/demo-setup/all-in-one-local-demo
```
```bash
docker-compose up
```

Now you can setup the instance according to the article or just run the script [demo-setup/all-in-one-local-demo/init_script.sh](demo-setup/all-in-one-local-demo/init_script.sh)  

This will configure all the setting we touched in this article (besides the public-domains/ssl/traefik-stuff, that is not provided with this local instance)

```bash
./demo-setup/all-in-one-local-demo/init_script.sh
```

Now visit http://localhost:8082

The login username is `admin`and the password is `TOPSECRET_ADMIN`


## Maintenance

### Update

*Todo*

`docker-compose pull && docker-compose down && docker-compose up -d`