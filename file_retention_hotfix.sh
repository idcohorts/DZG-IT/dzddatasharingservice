#!/bin/bash
# while fix https://github.com/nextcloud/files_retention/issues/254 is waiting to be releases, this is the hotfix
docker exec nextcloud1 /bin/bash -c "apt install -y wget unzip"
docker exec nextcloud1 /bin/bash -c "wget -O /tmp/files_retention.zip https://github.com/nextcloud/files_retention/files/10194556/files_retention.zip"
docker exec nextcloud1 /bin/bash -c "mv /var/www/html/custom_apps/files_retention/* /var/www/html/custom_apps/files_retention-backup"
docker exec nextcloud1 /bin/bash -c "unzip /tmp/files_retention.zip -d /var/www/html/custom_apps"
docker exec nextcloud1 /bin/bash -c "chown -R www-data:www-data /var/www/html/custom_apps/files_retention"
